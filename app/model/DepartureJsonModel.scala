package  model

  import play.api.libs.json._
  import play.api.libs.functional.syntax._

  object DepartureJsonModel {

    case class Departure(station: String, time: String, vehicle: String, platform: String, delay: String)

    implicit val departureReads: Reads[Departure] = (
      (JsPath \ "station").read[String] and
        (JsPath \ "time").read[String] and
        (JsPath \ "vehicle").read[String] and
        (JsPath \ "platform").read[String] and
        (JsPath \ "delay").read[String]
      )(Departure.apply _)

  }



