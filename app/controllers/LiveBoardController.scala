package controllers

import javax.inject.{Inject, _}
import model.DepartureJsonModel.Departure
import model.LiveBoardForm._
import play.api.data._
import play.api.libs.ws._
import play.api.mvc._

import scala.collection.mutable._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class LiveBoardController @Inject()(cc: MessagesControllerComponents, ws: WSClient) extends MessagesAbstractController(cc)  {

  val baseRequest: WSRequest = ws.url("https://api.irail.be/liveboard")
  private val departures = ArrayBuffer[Departure]()


  // URL to the method to calculate the departures.  You can call this directly from the template, but it
  // can be more convenient to leave the template completely stateless
  private val postURL = routes.LiveBoardController.calculateLiveboards()


  def liveboard() = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.liveboard(form, departures, postURL))
  }

  def directLiveboard(station: String, time: String) = Action { implicit request: MessagesRequest[AnyContent] =>
    val timeint = time.substring(0,2) ++ time.substring(3,5)
    val request = baseRequest.addQueryStringParameters("station" -> station,"time" -> timeint, "format" -> "json")
    val f = request.get().map({
      response =>
        val connectionsResult = (response.json \ "departures" \ "departure").get.validate[List[Departure]]
        departures.clear()
        connectionsResult.foreach(c => c.foreach(updateAndAddConnection))
    })

    Await.result(f, 30 second)
    Redirect(routes.LiveBoardController.liveboard()).flashing("Station" -> (station))
  }


  // This will be the action that handles our form post
  def calculateLiveboards() = Action { implicit request: MessagesRequest[AnyContent] =>
    val errorFunction = { formWithErrors: Form[Data] =>
      // This is the bad case, where the form had validation errors.
      // Let's show the user the form again, with the errors highlighted.
      // Note how we pass the form with errors to the template.
      BadRequest(views.html.liveboard(formWithErrors, departures, postURL))
    }

    val successFunction = { data: Data =>
      val request = baseRequest.addQueryStringParameters("station" -> data.station,"format" -> "json")
      val f = request.get().map({
        response =>
          val connectionsResult = (response.json \ "departures" \ "departure").get.validate[List[Departure]]
          departures.clear()
          connectionsResult.foreach(c => c.foreach(updateAndAddConnection))
      })

      Await.result(f, 30 second)
      Redirect(routes.LiveBoardController.liveboard()).flashing("Station" -> (data.station))
    }


    val formValidationResult =  form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }

  def updateAndAddConnection(departure: Departure) : Unit = {

    val departureTime = TimeUtils.getTime(departure.time.toLong)
    val vehicle =  departure.vehicle.substring(8)
    val delay = departure.delay.toInt.toString

    val updatedDepartures = departure.copy(station = departure.station, departureTime, vehicle, platform = departure.platform, delay)
    departures.append(updatedDepartures)
  }

}



